**Prueba técnica QA NTTData API**

Autor: Paul Ochoa

Se realiza la prueba número 2 de las API's mediante la herramienta postman a los servicios
- POST https://api.demoblaze.com/signup
- POST https://api.demoblaze.com/login

**Ejecutar proyecto**
1. Importar el archivo qanttdata.postman_collection.json en postman
2. Dentro se presentará 4 request para los diferentes escenarios
3. En la colección ejecutar la colección
4. Seleccionar los request a ejecutar, por defecto se seleccionan los 4

Si se desea ejecutar nuevamente, se deberá modificar en el Body el json de ingreso con datos nuevos para el requet signup para registar un nuevo usuario 
{"username" : "nombreusuario","password": "claveusuario" }
